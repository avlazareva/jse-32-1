package ru.t1.lazareva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.client.IAuthEndpointClient;
import ru.t1.lazareva.tm.dto.request.UserLoginRequest;
import ru.t1.lazareva.tm.dto.request.UserLogoutRequest;
import ru.t1.lazareva.tm.dto.request.UserViewProfileRequest;
import ru.t1.lazareva.tm.dto.response.UserLoginResponse;
import ru.t1.lazareva.tm.dto.response.UserLogoutResponse;
import ru.t1.lazareva.tm.dto.response.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("test2", "test2")).getSuccess());
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));

        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserViewProfileResponse viewProfile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}