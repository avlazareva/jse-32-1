package ru.t1.lazareva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.client.IProjectEndpointClient;
import ru.t1.lazareva.tm.command.AbstractCommand;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectEndpointClient getProjectEndpoint() {
        return getServiceLocator().getProjectEndpointClient();
    }


    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}