package ru.t1.lazareva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.client.ISystemEndpointClient;
import ru.t1.lazareva.tm.api.service.ICommandService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    protected ISystemEndpointClient getSystemEndpoint() {
        return serviceLocator.getSystemEndpointClient();
    }

}