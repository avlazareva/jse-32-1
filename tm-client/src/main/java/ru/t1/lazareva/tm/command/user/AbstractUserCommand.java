package ru.t1.lazareva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.client.IAuthEndpointClient;
import ru.t1.lazareva.tm.api.client.IUserEndpointClient;
import ru.t1.lazareva.tm.command.AbstractCommand;
import ru.t1.lazareva.tm.exception.entity.UserNotFoundException;
import ru.t1.lazareva.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpointClient();
    }

    @NotNull
    public IAuthEndpointClient getAuthEndpoint() {
        return serviceLocator.getAuthEndpointClient();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}