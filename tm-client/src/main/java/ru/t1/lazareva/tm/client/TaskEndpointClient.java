package ru.t1.lazareva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.client.ITaskEndpointClient;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser().getEmail());

        @NotNull final TaskEndpointClient taskClient = new TaskEndpointClient(authEndpointClient);
        System.out.println(taskClient.createTask(new TaskCreateRequest("TEST_NAME", "TEST_DESC")).getTask().toString());
        System.out.println(taskClient.showTaskList(new TaskListRequest()).getTasks());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskListResponse showTaskList(@NotNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse showTaskById(@NotNull final TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse showTaskByIndex(@NotNull final TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskListByProjectIdResponse showTaskListByProjectId(@NotNull final TaskListByProjectIdRequest request) {
        return call(request, TaskListByProjectIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindToProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindToProjectRequest request) {
        return call(request, TaskUnbindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

}