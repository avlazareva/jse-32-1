package ru.t1.lazareva.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sortType;

    public TaskListRequest(@Nullable final Sort sort) {
        this.sortType = sortType;
    }

}