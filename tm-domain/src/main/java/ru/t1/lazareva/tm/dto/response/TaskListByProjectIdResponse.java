package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<Task> tasks;

    public TaskListByProjectIdResponse(@NotNull final List<Task> tasks) {
        this.tasks = tasks;
    }

}
