package ru.t1.lazareva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    UserRegistryResponse registryUser(@NotNull UserRegistryRequest request);

    @NotNull
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull
    UserViewProfileResponse viewUserProfile(@NotNull final UserViewProfileRequest request);

}
