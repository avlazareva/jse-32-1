package ru.t1.lazareva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.service.IAuthService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.IUserService;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.AccessDeniedException;
import ru.t1.lazareva.tm.model.User;
import ru.t1.lazareva.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    public AuthService(@NotNull final IPropertyService propertyService, @NotNull final IUserService userService) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public User check(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

}