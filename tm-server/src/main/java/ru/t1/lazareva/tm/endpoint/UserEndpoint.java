package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.endpoint.IUserEndpoint;
import ru.t1.lazareva.tm.api.service.IAuthService;
import ru.t1.lazareva.tm.api.service.IServiceLocator;
import ru.t1.lazareva.tm.api.service.IUserService;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changeUserPassword(@NotNull final UserChangePasswordRequest request) {
        check(request);
        @Nullable final String id = request.getUserId();
        @Nullable final String password = request.getPassword();
        @NotNull final User user = getUserService().setPassword(id, password);
        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserViewProfileResponse viewUserProfile(@NotNull final UserViewProfileRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().findOneById(userId);
        return new UserViewProfileResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateUserProfile(@NotNull final UserUpdateProfileRequest request) {
        check(request);
        @Nullable final String id = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable final User user = getUserService().updateUser(id, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}
